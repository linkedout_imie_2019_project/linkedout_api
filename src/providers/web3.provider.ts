import Web3 from "web3";
import {readFileSync} from "fs";
import appRoot from "app-root-path";
import Contract from "web3/eth/contract";

const web3 = new Web3(process.env.WEB3_PATH || "ws://localhost:8545");

const contractPath = "D:/Documents/dev/linkedout/linkedout_eth/build/contracts/LinkedoutContract.json";

export const getWeb3Provider = () => {
    return web3;
};

export const getContract = (): Contract => {
    // console.log("app root", appRoot.path);
    const file = readFileSync(contractPath, "utf8");
    const jsonABI = JSON.parse(file);

    const linkedoutContract = new web3.eth.Contract(jsonABI);

    return linkedoutContract;
};