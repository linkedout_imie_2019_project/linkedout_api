import { model, Model, Schema, Document } from "mongoose";

export type CompanyModelType = Document & {
    name: string;
};

const CompanyModelSchema = new Schema({
    name: String,
}, { timestamps: true });

export const CompanyModelDb: Model<CompanyModelType> = model("CompanyModel", CompanyModelSchema);
