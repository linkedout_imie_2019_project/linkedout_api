import { model, Model, Schema, Document } from "mongoose";

export type TrainingModelType = Document & {
    name: string;
};

const TrainingModelSchema = new Schema({
    name: String,
}, { timestamps: true });

export const TrainingModelDb: Model<TrainingModelType> = model("TrainingModel", TrainingModelSchema);
