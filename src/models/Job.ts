import { model, Model, Schema, Document } from "mongoose";

export type JobModelType = Document & {
    name: string;
};

const JobModelSchema = new Schema({
    name: String,
}, { timestamps: true });

export const JobModelDb: Model<JobModelType> = model("JobModel", JobModelSchema);
