import { model, Model, Schema, Document } from "mongoose";

export type TrainingCenterModelType = Document & {
    name: string;
    adress: string;
};

const TrainingCenterModelSchema = new Schema({
    name: String,
    adress: String
}, { timestamps: true });

export const TrainingCenterModelDb: Model<TrainingCenterModelType> = model("TrainingCenterModel", TrainingCenterModelSchema);
