import { model, Model, Schema, Document } from "mongoose";

export type EmployeModelType = Document & {
    firstname: string;
    lastname: string;
};

const EmployeModelSchema = new Schema({
    firstname: String,
    lastname: String
}, { timestamps: true });

export const EmployeModelDb: Model<EmployeModelType> = model("EmployeModel", EmployeModelSchema);
